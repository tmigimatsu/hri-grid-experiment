#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

class MaxEntIRL:

    def __init__(self, num_states, num_actions, transition, feature, demonstrations):
        """
        Args:
            num_states                     (int): Number of states
            num_actions                    (int): Number of actions
            transition (int, int, int => double): Transition T(s_k | s_i, a_j) [s_i x a_j x s_k]
            feature             (int => [M x 1]): Feature f(s_i)
            demonstrations             ([D x T]): State trajectories
        """
        self.transition     = transition        # int, int, int => double
        self.feature        = feature           # int => [M x 1]
        self.demonstrations = demonstrations    # [D x T]

        self.N = num_states                     # Num states
        self.A = num_actions                    # Num actions
        self.D = self.demonstrations.shape[0]   # Num demonstrations
        self.T = self.demonstrations.shape[1]   # Num time steps
        self.M = self.feature(0).shape[0]       # Dim feature space

        # Vectorize transitions and features
        idx_states = np.arange(self.N, dtype=np.int)
        idx_actions = np.arange(self.A, dtype=np.int)

        # [N x A x N] matrix with T(s' | s_i, a_j) at entry (i, j, k)
        T_vec = np.vectorize(transition)
        self.T_sas = T_vec(*np.meshgrid(idx_states, idx_actions, idx_states, indexing="ij"))
        assert np.all(np.abs(np.sum(self.T_sas, axis=2) - 1) < 1e-5)

        # [M x N] matrix with f(s_i) in column i
        f_vec = lambda idx_states: np.column_stack([feature(i) for i in idx_states])
        self.f_s = f_vec(idx_states)

        # Demonstration feature counts [M x 1]
        # f_D = 1/D \sum_{\xi \in D} f_\xi
        #     = 1/D \sum_{s \in D}   f_s
        self.f_d = np.sum(f_vec(demonstrations.flatten()), axis=1) / self.D

    def exp_value_iteration(self, gamma=0.01, epsilon=0.01, max_iters=200):
        """
        Exponential value iteration: Find values

        Returns:
            Z_sa [N x A]: Exponential values
        """

        # [N x 1] vector with R(s_i; theta) at entry i
        R_s = self.theta.dot(self.f_s)

        # [N x 1] vector with V(s_i) at entry i
        V_s = np.zeros(self.N)

        # Value iteration
        for _ in range(max_iters):
            V_s_prev = V_s

            # V(s_i) = \max_a \sum_{s_k} T(s_k | s_i, a_j) * (R(s_k) + \gamma * V(s_k))
            V_sa = np.tensordot(self.T_sas, R_s + gamma * V_s, axes=(2,0))
            V_s = np.max(V_sa, axis=1)

            # Check for convergence
            if np.all(np.abs(V_s - V_s_prev) < epsilon):
                break

        # [N x A] matrix with exponential value exp(V(s_i, a_j)) at entry (i, j)
        # Subtract maximum value at each state for numerical stability
        Z_sa = np.exp(V_sa - np.max(V_sa, axis=1)[:,None])

        return Z_sa

    def backward_pass(self):
        """
        Backward pass: Approximate values (alternative to value iteration)

        Returns:
            Z_sa [N x A]: Exponential value
        """

        # [N x 1] vector with R(s_i; theta) at entry i
        R_s = self.theta.dot(self.f_s)

        # [N x 1] vector with exp(R(s_i; theta)) at entry i
        # Subtract maximum reward for numerical stability
        exp_R_s = np.exp(R_s - np.max(R_s))

        # [N x 1] vector with rewards of paths starting from s_i, at entry i
        Z_s = np.ones(self.N)

        for t in range(self.T):
            # [N x A] matrix with rewards of paths starting from (s_i, a_j), at entry (i,j)
            # Z_{s_i,a_j} = exp(R(s_i; theta)) * \sum_k T(s_k | s_i, a_j) Z_{s_k}
            Z_sa = exp_R_s[:,None] * np.tensordot(self.T_sas, Z_s, axes=(2,0))

            # Z_{s_i} = \sum_{a_j} Z_{s_i,a_j}
            Z_s = np.sum(Z_sa, axis=1)

        return Z_sa

    def forward_pass(self, P_sa):
        """
        Forward pass: Find state probabilities P(s)
        
        Args:
            P_sa [N x A]: Current policy P(s,a)

        Returns:
            D_s [M x 1]: State probabilities over trajectories
        """

        # [T x N] matrix with P_t(s_i) at entry (i,t)
        D_ts = np.zeros((self.T, self.N))

        # Get initial probabilities P_0(s_i) from demonstrations
        initial_states, counts = np.unique(demonstrations[:,0], return_counts=True)
        D_ts[0,initial_states] = counts / self.D

        for t in range(1, self.T):
            # Line 5 from Ziebart doesn't work as written:
            # D_{s_i,t+1} = \sum_{a_j} \sum_{s_k} D_{s_k,t} P(a_j | s_i) T(s_k | s_i, a_j)
            # D_ts[t,:] = np.sum(np.tensordot(self.T_sas, D_ts[t-1,:], axes=(2,0)) * P_sa, axis=1)

            # D_{s_i,t+1} = \sum_{a_j} \sum_{s_k} D_{s_k,t} P(a_j | s_k) T(s_i | s_k, a_j)
            D_ts[t,:] = np.sum(np.sum(self.T_sas * P_sa[:,:,None], axis=1) * D_ts[t-1,:,None], axis=0)

        # [N x 1] vector with trajectory state frequencies P(s_i) at entry i
        D_s = np.sum(D_ts, axis=0)

        return D_s

    def expected_feature_counts(self):
        """
        Expected feature counts

        Returns:
            E_f [M x 1]: Expected feature counts over trajectories
        """

        # Backward pass: Find values
        # [N x A] matrix with exponential values V(s_i, a_j) at entry (i,j)
        Z_sa = self.backward_pass()
        # Z_sa = self.exp_value_iteration()

        # Local action probability computation: Find policy
        # [N x A] matrix with policy P(a_j | s_i) at entry (i,j)
        # P(a_j | s_i) = Z_{s_i,a_j} / Z_{s_i}
        P_sa = Z_sa / np.sum(Z_sa, axis=1)[:,None]

        # Forward pass: Find state probabilities
        # [N x 1] vector with trajectory state frequencies P(s_i) at entry i
        D_s = self.forward_pass(P_sa)

        # Expected feature counts [M x 1]
        # E_f = \sum_{s_i} D_{s_i} f_{s_i}
        E_f = self.f_s.dot(D_s)

        return E_f

    def gradient_descent(self, epsilon=0.01, alpha=0.1, max_iters=2000, debug=False):
        """
        Gradient descent: Find theta

        Args:
            epsilon   (scalar): Convergence tolerance
            alpha     (scalar): Gradient step size
            max_iters (scalar): Maximum number of gradient steps
            debug       (bool): Print debug information
        """

        self.theta = np.random.rand(self.M)

        for _ in range(max_iters):

            # Expected feature counts
            E_f = self.expected_feature_counts()

            # Gradient: difference between deonstration and expected feature counts
            grad_L = self.f_d - E_f

            # Gradient ascent
            self.theta += alpha * grad_L

            # Check for convergence
            if np.all(np.abs(grad_L) < epsilon):
                break

            if debug:
                print("f_D: {}\nE_f: {}\ngrad_L: {}\ntheta: {}\n".format(self.f_d, E_f, grad_L, self.theta))

        # Return value, policy, and reward for debugging
        Z_sa = self.backward_pass()
        Z_s = np.sum(Z_sa, axis=1)[:,None]
        P_s = np.argmax(Z_sa, axis=1)
        R_s = self.theta.dot(self.f_s)

        return self.theta, Z_s, P_s, R_s


if __name__ == '__main__':

    # Grid world example
    size_grid = (5, 5)
    N = size_grid[0] * size_grid[1]     # Num states
    A = 4                               # Num actions
    T = size_grid[0] + size_grid[1] - 1 # Num time steps

    # Demonstration state trajectories [D x T]
    D = 5  # Num demonstrations
    demonstrations = np.zeros((D, T), dtype=np.int)
    demonstration = [(y,0) for y in range(size_grid[0])] + [(size_grid[0]-1,x) for x in range(1,size_grid[1])]
    demonstrations[0,:] = np.ravel_multi_index(list(zip(*demonstration)), size_grid)
    demonstration = [(0,0)] + [(y,1) for y in range(size_grid[0])] + [(size_grid[0]-1,x) for x in range(2,size_grid[1])]
    demonstrations[1,:] = np.ravel_multi_index(list(zip(*demonstration)), size_grid)
    demonstration = [(y,0) for y in range(2)] + [(y,1) for y in range(1,size_grid[0])] + [(size_grid[0]-1,x) for x in range(2,size_grid[1])]
    demonstrations[2,:] = np.ravel_multi_index(list(zip(*demonstration)), size_grid)
    demonstration = [(y,0) for y in range(3)] + [(y,1) for y in range(2,size_grid[0])] + [(size_grid[0]-1,x) for x in range(2,size_grid[1])]
    demonstrations[3,:] = np.ravel_multi_index(list(zip(*demonstration)), size_grid)
    demonstration = [(y,0) for y in range(4)] + [(y,1) for y in range(3,size_grid[0])] + [(size_grid[0]-1,x) for x in range(2,size_grid[1])]
    demonstrations[4,:] = np.ravel_multi_index(list(zip(*demonstration)), size_grid)

    def transition(idx_state, idx_action, idx_next_state):
        """
        Args:
            idx_state      (int): state index i
            idx_action     (int): action index j
            idx_next_state (int): state index k

        Returns:
            T(s_k | s_i, a_j) (double): Transition probability
        """

        (y, x) = np.unravel_index([idx_state, idx_next_state], size_grid)
        if idx_action == 0: # up
            if x[0] != x[1] or y[1] < 0:
                return 0
            if y[1] == 0 and y[0] == 0:
                return 1
            if y[1] == y[0] - 1:
                return 1
        elif idx_action == 1: # down
            if x[0] != x[1] or y[1] >= size_grid[0]:
                return 0
            if y[1] == size_grid[0] - 1 and y[0] == size_grid[0] - 1:
                return 1
            if y[1] == y[0] + 1:
                return 1
        elif idx_action == 2: # left
            if y[0] != y[1] or x[1] < 0:
                return 0
            if x[1] == 0 and x[0] == 0:
                return 1
            if x[1] == x[0] - 1:
                return 1
        elif idx_action == 3: # right
            if y[0] != y[1] or x[1] >= size_grid[1]:
                return 0
            if x[1] == size_grid[1] - 1 and x[0] == size_grid[1] - 1:
                return 1
            if x[1] == x[0] + 1:
                return 1
        return 0

    def feature(idx_state):
        """
        Args:
            state (int): state index i

        Returns:
            f(s_i) [M x 1]: Feature vector
        """

        f = np.zeros(N)
        f[idx_state] = 1
        return f

    irl = MaxEntIRL(N, A, transition, feature, demonstrations)
    theta, V_s, P_s, R_s = irl.gradient_descent(alpha=1, debug=True)
    V_s = V_s.reshape(size_grid)
    P_s = P_s.reshape(size_grid)
    R_s = R_s.reshape(size_grid)
    print(P_s)

    # Visualize demonstrated paths
    states, counts = np.unique(demonstrations.flatten(), return_counts=True)
    D_s = np.zeros(N)
    D_s[states] = counts
    D_s = D_s.reshape(size_grid)
 
    plt.figure()
    ax = plt.subplot(1, 3, 1)
    cax = ax.matshow(D_s)
    plt.colorbar(cax)
    plt.title("Demonstrated state frequencies")
    ax = plt.subplot(1, 3, 2)
    cax = ax.matshow(R_s)
    plt.colorbar(cax)
    plt.title("Learned reward")
    ax = plt.subplot(1, 3, 3)
    cax = ax.matshow(np.log(V_s), cmap=plt.cm.Blues)
    plt.colorbar(cax)
    for x in range(size_grid[0]):
        for y in range(size_grid[1]):
            if P_s[y,x] == 0:
                ax.quiver(x,y,0,1)
            elif P_s[y,x] == 1:
                ax.quiver(x,y,0,-1)
            elif P_s[y,x] == 2:
                ax.quiver(x,y,-1,0)
            elif P_s[y,x] == 3:
                ax.quiver(x,y,1,0)
    plt.title("Expected value")

    plt.show()
