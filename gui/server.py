#!/usr/bin/env python
"""
server.py

Author: Toki Migimatsu
Created: April 2017
"""

from __future__ import print_function, division
from argparse import ArgumentParser
import json
import sys
import os
import shutil
import threading

from HTTPRequestHandler import makeHTTPRequestHandler, ThreadingHTTPServer

file_lock = threading.Lock()

def handle_get_request(request_handler, get_vars, **kwargs):
    """
    HTTPRequestHandler callback:

    Serve content inside WEB_DIRECTORY
    """
    WEB_DIRECTORY = "web"
    path_tokens = [token for token in request_handler.path.split("/") if token]

    # Default to index.html
    if not path_tokens or ".." in path_tokens:
        request_path = "index.html"
    else:
        request_path = os.path.join(*path_tokens)
    request_path = os.path.join(WEB_DIRECTORY, request_path)

    # Check if file exists
    if not os.path.isfile(request_path):
        request_handler.send_error(404, "File not found.")
        return

    # Otherwise send file directly
    with open(request_path, "rb") as f:
        shutil.copyfileobj(f, request_handler.wfile)

def handle_post_request(request_handler, post_vars, **kwargs):
    """
    HTTPRequestHandler callback:

    Set POST variables as Redis keys
    """

    print(post_vars)
    for key, val_str in post_vars.items():
        with open("data.log", "a") as f:
            file_lock.acquire()
            log_entry = val_str[0]
            if not isinstance(log_entry, str):
                log_entry = log_entry.decode("utf-8")
            f.write(log_entry + "\n")
            file_lock.release()


if __name__ == "__main__":
    # Parse arguments
    parser = ArgumentParser(description=(
        "HRI Grid."
    ))
    parser.add_argument("-p", "--http_port", help="HTTP Port (default: 8000)", default=8000, type=int)
    args = parser.parse_args()

    # Create HTTPServer
    http_server = ThreadingHTTPServer(("", args.http_port), makeHTTPRequestHandler(handle_get_request, handle_post_request))

    # Start HTTPServer
    print("Started HTTP server on port %d" % (args.http_port))
    http_server.serve_forever()
