kBlockSize = 50;
kGridSize = 50;
kWallWidth = 6;
kWallHeight = 4;
kNumBlocks = kWallWidth * kWallHeight;
kMaxIterations = kNumBlocks * 10;

kBlockColors = [
	"#ff8080"
	// "#ffff80",
	// "#80ff80",
	// "#80ffff",
	// "#8080ff",
	// "#ff80ff"
];
kBorderColors = [
	"#aa0000"
	// "#aaaa00",
	// "#00aa00",
	// "#00aaaa",
	// "#0000aa",
	// "#aa00aa"
];
kNumColors = kBlockColors.length;

function checkCollisions(corners_x, corners_y, predicate) {
	for (var x = 0; x < corners_x.length; x++) {
		for (var y = 0; y < corners_y.length; y++) {
			var $blocks = $(document.elementsFromPoint(corners_x[x], corners_y[y]));

			for (var i = 0; i < $blocks.length; i++) {
				if (predicate($blocks.eq(i))) return true;
			}
		}
	}
	return false;
}

function loadWorld() {

	$("body").html("");

	// Render floor and wall outline
	var center_x = Math.floor(window.innerWidth / 2 / kBlockSize) * kBlockSize;
	var center_y = Math.floor(window.innerHeight / 2 / kBlockSize) * kBlockSize;
	var wall_left = center_x - kWallWidth * kBlockSize / 2;
	var wall_top = center_y - kWallHeight * kBlockSize / 2;
	var $floor = $(document.createElement("div"))
		.addClass("floor")
		.css({
			"top": wall_top + kWallHeight * kBlockSize,
			"left": wall_left - 100
		});
	var $wall = $(document.createElement("div"))
		.addClass("wall")
		.css({
			"top": wall_top,
			"left": wall_left
		});
	var $padding = $(document.createElement("div"))
		.addClass("padding")
		.css({
			"top": wall_top - 50,
			"left": wall_left - 150
		});
	$("body").append($padding).append($wall).append($floor);

	// Generate blocks
	block_positions = [];
	for (var i = 0; i < kNumBlocks; i++) {
		// Find non-overlapping positions
		var top, left;
		for (var j = 0; j < kMaxIterations; j++) {
			top = Math.floor(Math.random() * ($(window).height() - kBlockSize) / kGridSize) * kGridSize;
			left = Math.floor(Math.random() * ($(window).width() - kBlockSize) / kGridSize) * kGridSize;
			var corners_x = [left - 1, left + kBlockSize / 2, left + kBlockSize];
			var corners_y = [top - 1, top + kBlockSize / 2, top + kBlockSize];
			var is_occupied = checkCollisions(corners_x, corners_y, function($block) {
				return $block.is("div");
			})
			if (!is_occupied) break;
		}
		block_positions.push({
			top: top,
			left: left
		});

		// Create block element
		var $block = $(document.createElement("div"))
			.addClass("grabbable block")
			.css({
				"background-color": kBlockColors[i % kNumColors],
				"border-color": kBorderColors[i % kNumColors],
				"top": top,
				"left": left
			});

		$("body").append($block);
	}

	// Define block behavior
	drag_log = [];
	$(".block")
		.draggable({
			grid: [ kGridSize, kGridSize ],
			drag: function() {
				var $this = $(this);

				// Check support
				var corners_x = [drag_start.left + kBlockSize / 2, drag_start.left + kBlockSize / 2 - 1];
				var corners_y = [drag_start.top - 1];
				var is_supporting = checkCollisions(corners_x, corners_y, function($block) {
					return $block.get(0) != $this.get(0) && $block.hasClass("block");
				});
				if (is_supporting) return false;
			}
		})

		.on("mousedown", function(e) {
			var $this = $(this);
			drag_start = $this.position();

			$this.addClass("active");
		})

		.on("mouseup", function() {
			var $this = $(this);
			var drag_end = $this.position();

			// Find dragged block - $(this) only returns the topmost block
			var $blocks = $(document.elementsFromPoint(drag_end.left, drag_end.top));
			for (var i = 0; i < $blocks.length; i++) {
				if (!$blocks.eq(i).hasClass("active")) continue;
				$this = $blocks.eq(i);
			}
			$this.removeClass("active");

			// Check wall
			var corners_x = [drag_end.left];
			var corners_y = [drag_end.top];
			var is_colliding = checkCollisions([drag_end.left], [drag_end.top], function($block) {
				return $block.hasClass("wall");
			}) && checkCollisions([drag_end.left + kBlockSize - 2], [drag_end.top], function($block) {
				return $block.hasClass("wall");
			}) && checkCollisions([drag_end.left], [drag_end.top + kBlockSize], function($block) {
				return $block.hasClass("wall");
			}) && checkCollisions([drag_end.left + kBlockSize - 2], [drag_end.top + kBlockSize], function($block) {
				return $block.hasClass("wall");
			});
			if (!is_colliding) {
				$this.offset(drag_start);
				return;
			}

			// Avoid collisions
			var corners_x = [drag_end.left, drag_end.left + kBlockSize - 1];
			var corners_y = [drag_end.top, drag_end.top + kBlockSize - 1];
			var is_colliding = checkCollisions(corners_x, corners_y, function($block) {
				return $block.get(0) != $this.get(0) && $block.hasClass("block");
			});
			if (is_colliding) {
				$this.offset(drag_start);
				return;
			}

			// Check gravity
			var corners_x = [drag_end.left + kBlockSize / 2, drag_end.left + kBlockSize / 2 - 1];
			var corners_y = [drag_end.top + kBlockSize + 1];
			var is_supported = checkCollisions(corners_x, corners_y, function($block) {
				return $block.hasClass("block") || $block.hasClass("floor");
			});
			if (!is_supported) {
				$this.offset(drag_start);
				return;
			}

			// Valid placement - record data
			drag_log.push({
				drag_start: drag_start,
				drag_end: drag_end
			});

			// Check if board is complete
			var $wall = $(".wall");
			var is_finished = true;
			for (var x = $wall.position().left + kBlockSize / 2; x < $wall.position().left + $wall.width(); x += kBlockSize) {
				for (var y = $wall.position().top + kBlockSize / 2; y < $wall.position().top + $wall.height(); y += kBlockSize) {
					var is_occupied = checkCollisions([x], [y], function($block) {
						return $block.hasClass("block");
					});
					if (!is_occupied) {
						is_finished = false;
						break;
					}
				}
				if (!is_finished) break;
			}
			if (is_finished) {
				$.post("/", {
					data: JSON.stringify({
						username: username,
						drag_log: drag_log,
						block_positions: block_positions
					})
				});
				if (confirm("You win!!! Play again?")) {
					loadWorld();
				}
			}
		});
}

$(document).ready(function() {

	loadWorld();

	// Prompt user for name
	username = prompt("What is your name?");
	while (username == null || username == "") {
		username = prompt("Please enter in a name.");
	}
});
